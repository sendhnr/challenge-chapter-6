const { Users } = require('../models');
const bcrypt = require('bcrypt')

async function getAllUsers(req, res){
    try {
        const user = await Users.findAll();
        res.status(200).json({
            'test': 'success',
            'data': user
        })
    } catch(err) {
        console.log(err.message)
    }
}

//create Users
async function addUsers(req, res) {
    const {username, email, password, role} = req.body;
    const hashedPassword = await bcrypt.hash(password, 10)
    try {
        const newUser = await Users.create({
            username: username,
            email: email,
            password: hashedPassword,
            role: role,
        })
        res.status(200).json({
            'test': 'registered success!',
            'data': newUser
        })
    } catch(err) {
        res.status(400).json({msg: err.message})
    }
}

//update User
async function updateUsers(req, res) {
    try {
        const id = req.params.id
        const user = await Users.findOne({
            where: {
                id: id
            }
        });
        if (!user) return res.status(404).json({msg: `user not found`})
        const {username, email, password, role} = req.body;
        const newUser = await Users.update({
            username,
            email,
            password,
            role,
        }, { 
            where: { id: id }
        });
        res.status(200).json({
            'test': 'update success!',
            'data': newUser
      });
    } catch (err) {
      console.log(err.message);
    }
}

//delete user
async function deleteUsers(req, res) {
    try {
        const id = req.params.id
        if(!user){
            res.status(404).json({msg: `user not found`})
        } else{
            const newUser = await Users.destroy({ 
                where: { id: id }
            });
            res.status(200).json({
                'test': 'deleted success!',
          });
        }
        const user = await Users.findOne({
            where: {
                id: id
            }
        });
    } catch (err) {
      console.log(err.message);
    }
}

module.exports = {
    getAllUsers,
    addUsers,
    updateUsers,
    deleteUsers
}