const { Users, Cars } = require('../models')
const bcrypt = require('bcrypt')

//register
async function register(req, res) {
    const {username, email, password, role} = req.body;
    const hashedPassword = await bcrypt.hash(password, 10)
    try {
        const newUser = await Users.create({
            username: username,
            email: email,
            password: hashedPassword,
            role: role,
        })
        res.status(200).json({
            'test': 'register berhasil',
            'data': newUser
        })
    } catch(err) {
        res.status(400).json({msg: err.message})
    }
}

module.exports = {
    register,
}