const { Cars } = require('../models');

async function getCars(req, res){
    try {
        const car = await Cars.findAll();
        res.status(200).json({
            'test': '123 kita semua',
            'data': car
        })
    } catch(err) {
        console.log(err.message)
    }
}

//create new cars
async function addCars(req, res) {
    try {
        const {name, price, size, image, userId} = req.body;
        const newCars = await Cars.create({
            name,
            price,
            size,
            image,
            userId
        })
        res.status(200).json({
            'test': '123',
            'data': newCars
        })
    } catch(err) {
        console.log(err.message)
    }
}

module.exports = {
    getCars,
    addCars
}