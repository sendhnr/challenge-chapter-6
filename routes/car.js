const router = require('express').Router();

const carController = require('../controllers/carController');

router.get('/api/cars', carController.getCars)
router.post('/api/cars', carController.addCars)

module.exports = router