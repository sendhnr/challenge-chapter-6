const router = require('express').Router();

const authController = require('../controllers/authController');

router.get('/api/register', authController.register)

module.exports = router