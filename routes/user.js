const router = require('express').Router();

const userController = require('../controllers/userController');

router.get('/api/users', userController.getAllUsers)
router.post('/api/users', userController.addUsers)
router.patch('/api/users/:id', userController.updateUsers)
router.delete('/api/users/:id', userController.deleteUsers)

module.exports = router