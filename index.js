require('dotenv').config();
const express = require('express');
const cors = require('cors')
const session = require('express-session')

//third-party middleware
// const bodyParser = require('body-parser');
// const path = require('path');

//initializations
const app = express()

//set JSON
app.use(express.json());
app.use(express.urlencoded({extended: true}));
//express json

//our routes
const carRouter = require('./routes/car');
const userRouter = require('./routes/user');
const authRouter = require('./routes/auth');

const PORT = process.env.PORT
//routes
app.use(carRouter);
app.use(userRouter);
app.use(authRouter);

app.use(cors)

app.listen(PORT, () => {
    console.log(`Server started on ${PORT}`);
});