'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Users, {as: 'Users', foreignKey: 'usernameId'});
    }
  }
  Cars.init({
    name: DataTypes.STRING,
    price: DataTypes.STRING,
    size: DataTypes.STRING,
    image: DataTypes.TEXT,
    usernameId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Cars',
  });
  return Cars;
};